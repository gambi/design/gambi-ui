"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Spinner = exports.Slider = exports.Input = void 0;

var _react = _interopRequireWildcard(require("react"));

require("./Input.css");

var _BottomDrawer = _interopRequireDefault(require("../BottomDrawer/BottomDrawer"));

var _Button = _interopRequireDefault(require("../Button/Button"));

var _List = _interopRequireDefault(require("../List/List"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Input = /*#__PURE__*/function (_Component) {
  _inherits(Input, _Component);

  var _super = _createSuper(Input);

  function Input(props) {
    var _this;

    _classCallCheck(this, Input);

    _this = _super.call(this, props);

    if (_this.props.callback) {
      _this.cb = _this.props.callback.bind(_assertThisInitialized(_this));
    } else {
      _this.cb = _this.emptyFn.bind(_assertThisInitialized(_this));
    }

    return _this;
  }

  _createClass(Input, [{
    key: "emptyFn",
    value: function emptyFn(val) {
      console.warn(val);
    }
  }, {
    key: "handleChange",
    value: function handleChange(event) {
      this.cb(event.target.value);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this,
          _React$createElement;

      if (this.props.placeholder != null || this.props.placeholder == "") {
        this.placeholder = "";
      } else {
        this.placeholder = this.props.placeholder;
      }

      if (this.props.black) {
        this.theme = "gd-color-black";
      } else {
        this.theme = "gd-color-primary";
      }

      if (this.props.secure) {
        this.type = "password";
      } else if (this.props.num) {
        this.type = "number";
      } else {
        this.type = "text";
      }

      return /*#__PURE__*/_react.default.createElement("input", (_React$createElement = {
        type: "text",
        className: "gd-text-input ".concat(this.theme),
        placeholder: this.props.placeholder
      }, _defineProperty(_React$createElement, "type", this.type), _defineProperty(_React$createElement, "onChange", function onChange(val) {
        _this2.handleChange(val);
      }), _React$createElement));
    }
  }]);

  return Input;
}(_react.Component);

exports.Input = Input;

var Spinner = /*#__PURE__*/function (_Component2) {
  _inherits(Spinner, _Component2);

  var _super2 = _createSuper(Spinner);

  function Spinner(props) {
    var _this3;

    _classCallCheck(this, Spinner);

    _this3 = _super2.call(this, props);
    _this3.state = {
      open: false,
      val: [],
      listKey: Math.floor(Math.random() * 999999)
    };
    _this3.onFinish = _this3.closeSpinner.bind(_assertThisInitialized(_this3));
    _this3.onStart = _this3.openSpinner.bind(_assertThisInitialized(_this3));
    _this3.cols = _this3.props.cols;

    if (_this3.props.title) {
      _this3.title = _this3.props.title;
    } else {
      _this3.title = "Open";
    }

    _this3.cb = _this3.updateVal.bind(_assertThisInitialized(_this3));

    if (_this3.props.multi) {
      _this3.mi = _this3.props.multi;
    } else {
      _this3.mi = [];
    }

    if (_this3.props.onUpdate) {
      _this3.callback = _this3.props.onUpdate.bind(_assertThisInitialized(_this3));
    } else {
      _this3.callback = _this3.emptyFun.bind(_assertThisInitialized(_this3));
    }

    return _this3;
  }

  _createClass(Spinner, [{
    key: "emptyFun",
    value: function emptyFun() {
      console.warn("Update Method not Implemented");
    }
  }, {
    key: "closeSpinner",
    value: function closeSpinner() {
      this.callback(this.state.val);
      this.setState({
        open: false
      });
      var a = setTimeout(function () {
        this.setState({
          listKey: Math.floor(Math.random() * 999999)
        });
      }.bind(this), 1050);
    }
  }, {
    key: "openSpinner",
    value: function openSpinner() {
      if (this.props.clearOnOpen) {
        var a = this.state.val;

        while (a.length) {
          a.pop();
        }

        console.log(a);
        this.setState({
          open: true,
          val: a
        });
      }

      this.setState({
        open: true
      });
    }
  }, {
    key: "updateVal",
    value: function updateVal(index, val) {
      var aval = this.state.val;

      if (val[0] !== this.state.val[0] && val[0] !== undefined) {
        aval[index] = val;
        this.state.val = aval;
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      return /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(_Button.default, {
        black: true,
        outline: true,
        onClick: this.onStart
      }, this.title), /*#__PURE__*/_react.default.createElement(_BottomDrawer.default, {
        open: this.state.open,
        onFinished: this.onFinish,
        key: this.state.listKey
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-spinner-container"
      }, this.props.cols.map(function (col, index) {
        return /*#__PURE__*/_react.default.createElement("div", {
          className: "gd-spinner-col",
          key: index
        }, /*#__PURE__*/_react.default.createElement(_List.default, {
          options: col,
          selectable: true,
          multi: _this4.mi.includes(index),
          onUpdate: function onUpdate(val) {
            _this4.cb(index, val);
          }
        }));
      }))));
    }
  }]);

  return Spinner;
}(_react.Component);

exports.Spinner = Spinner;

var Slider = /*#__PURE__*/function (_Component3) {
  _inherits(Slider, _Component3);

  var _super3 = _createSuper(Slider);

  function Slider(props) {
    var _this5;

    _classCallCheck(this, Slider);

    _this5 = _super3.call(this, props);

    _this5.setState({
      value: (_this5.props.max + _this5.props.min) / 2
    });

    _this5.onChange = _this5.onChange.bind(_assertThisInitialized(_this5));
    _this5.listen = props.onUpdate.bind(_assertThisInitialized(_this5));
    return _this5;
  }

  _createClass(Slider, [{
    key: "onChange",
    value: function onChange(event) {
      this.listen(event.target.value);
      this.setState({
        value: event.target.value
      });
    }
  }, {
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "slidecontainer"
      }, /*#__PURE__*/_react.default.createElement("input", {
        type: "range",
        min: this.props.min,
        max: this.props.max,
        value: this.state.value,
        className: "slider",
        onChange: this.onChange
      }));
    }
  }]);

  return Slider;
}(_react.Component);

exports.Slider = Slider;