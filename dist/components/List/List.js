"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ListItem = exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

require("./List.css");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function () { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var ListItem = /*#__PURE__*/function (_Component) {
  _inherits(ListItem, _Component);

  var _super = _createSuper(ListItem);

  function ListItem(props) {
    var _this;

    _classCallCheck(this, ListItem);

    _this = _super.call(this, props);
    _this.onClick = _this.props.onClick.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(ListItem, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      return /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-list-item ".concat(this.props.active ? "gd-list-item-selected" : ""),
        onClick: function onClick() {
          return _this2.onClick(_this2.props.title);
        }
      }, this.props.active ? /*#__PURE__*/_react.default.createElement("span", {
        className: "material-icons"
      }, "done") : "", /*#__PURE__*/_react.default.createElement("span", {
        className: "gd-list-item-title"
      }, this.props.title), " \xA0\xA0\xA0\xA0\xA0", /*#__PURE__*/_react.default.createElement("span", null, this.props.children));
    }
  }]);

  return ListItem;
}(_react.Component);

exports.ListItem = ListItem;

var List = /*#__PURE__*/function (_Component2) {
  _inherits(List, _Component2);

  var _super2 = _createSuper(List);

  function List(props) {
    var _this3;

    _classCallCheck(this, List);

    _this3 = _super2.call(this, props);

    _defineProperty(_assertThisInitialized(_this3), "state", {
      multiSelect: false,
      updates: 0,
      selected: []
    });

    _this3.oc = _this3.itemClicked.bind(_assertThisInitialized(_this3));

    if (_this3.props.onUpdate != null) {
      _this3.onUpdate = _this3.props.onUpdate.bind(_assertThisInitialized(_this3));
    } else {
      _this3.onUpdate = _this3.blankFn.bind(_assertThisInitialized(_this3));
    }

    if (_this3.props.addElements) {
      _this3.addElements = _this3.props.addElements.bind(_assertThisInitialized(_this3));
    } else {
      _this3.addElements = _this3.blankButtons.bind(_assertThisInitialized(_this3));
    }

    return _this3;
  }

  _createClass(List, [{
    key: "filterRemove",
    value: function filterRemove(list, val) {
      var filteredItems = list.filter(function (item) {
        return item !== val;
      });
      return filteredItems;
    }
  }, {
    key: "itemClicked",
    value: function itemClicked(value) {
      if (this.props.selectable) {
        if (this.props.multi) {
          if (this.state.selected.includes(value)) {
            this.setState({
              selected: this.filterRemove(this.state.selected, value),
              updates: this.state.updates + 1
            });
          } else {
            this.state.selected.push(value);
            this.setState({
              updates: this.state.updates + 1
            });
          }
        } else {
          if (this.state.selected.includes(value)) {
            this.setState({
              selected: []
            });
          } else {
            this.setState({
              selected: value
            });
          }
        }
      }

      this.onUpdate(value);
    }
  }, {
    key: "blankFn",
    value: function blankFn() {
      console.warn("List on click method not implemented");
    }
  }, {
    key: "blankButtons",
    value: function blankButtons() {}
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      return /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-list"
      }, this.props.options.map(function (option, index) {
        return /*#__PURE__*/_react.default.createElement(ListItem, {
          key: option,
          active: _this4.state.selected.includes(option),
          onClick: _this4.oc,
          title: option
        }, _this4.addElements(index, option));
      }));
    }
  }]);

  return List;
}(_react.Component);

exports.default = List;