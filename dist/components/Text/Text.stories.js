"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.p = exports.h4 = exports.h3 = exports.h2 = exports.h1 = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _GambiApp = _interopRequireDefault(require("../../page/App/GambiApp"));

var _Text = require("./Text");

var _react2 = require("@storybook/react");

var _addonInfo = require("@storybook/addon-info");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _react2.addDecorator)(_addonInfo.withInfo);
var _default = {
  title: 'Text',
  decorators: [_addonInfo.withInfo]
};
exports.default = _default;

var h1 = function h1() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Text.H1, null, "Header 1"));
};

exports.h1 = h1;

var h2 = function h2() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Text.H2, null, "Header 2"));
};

exports.h2 = h2;

var h3 = function h3() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Text.H3, null, "Header 3"));
};

exports.h3 = h3;

var h4 = function h4() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Text.H4, null, "Header 4"));
};

exports.h4 = h4;

var p = function p() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Text.P, null, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum laoreet urna quis nulla gravida varius. In aliquet tristique enim at bibendum. Sed lectus diam, aliquam id hendrerit sit amet, interdum non mauris. Suspendisse interdum turpis in quam interdum, vitae iaculis sapien venenatis. Donec non sem eu augue volutpat auctor. Quisque ac lacus vel metus finibus viverra. Donec ac sagittis ante, sed suscipit dui. Curabitur sit amet consectetur mauris. Nulla facilisi. Duis sed vestibulum massa. Curabitur sodales iaculis viverra.", /*#__PURE__*/_react.default.createElement("br", null), /*#__PURE__*/_react.default.createElement("br", null), "Phasellus orci nulla, mollis in dolor tincidunt, mollis posuere ipsum. Pellentesque at pulvinar quam, vitae aliquet velit. Aenean commodo sapien quis turpis faucibus, vitae mollis orci aliquam. Proin nulla odio, malesuada in ultrices vel, porttitor tristique felis. Sed tempus, purus nec sollicitudin blandit, felis lacus bibendum odio, non pellentesque justo diam eget lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam erat volutpat. Phasellus in cursus libero, in auctor felis. Quisque volutpat, velit vel tristique interdum, velit odio rhoncus turpis, quis vehicula magna arcu id odio. Sed auctor massa vitae felis scelerisque, quis consequat odio rhoncus. Vestibulum laoreet est enim. Nam accumsan feugiat semper. Praesent tincidunt faucibus neque, at malesuada ex dictum vitae. Duis placerat mollis arcu eget suscipit.", /*#__PURE__*/_react.default.createElement("br", null), /*#__PURE__*/_react.default.createElement("br", null), "Quisque mauris ligula, condimentum at sapien in, tempus mollis dolor. Aliquam erat volutpat. Morbi venenatis auctor odio, ac ultrices justo rhoncus posuere. Pellentesque sit amet velit dignissim, viverra tellus eget, vulputate ex. Proin iaculis a mi eget dignissim. Praesent risus sapien, pellentesque sed orci sed, mattis faucibus metus. Etiam commodo purus non purus iaculis, vitae tincidunt ante interdum. Nulla scelerisque facilisis tortor sed laoreet. Ut orci ligula, sodales id elementum id, maximus in elit. Cras sodales condimentum dictum. Quisque egestas dolor velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.", /*#__PURE__*/_react.default.createElement("br", null), /*#__PURE__*/_react.default.createElement("br", null), "Cras et libero nec elit laoreet tincidunt at accumsan justo. Vestibulum diam nisi, convallis in condimentum quis, auctor nec urna. Aenean volutpat accumsan nisi sed varius. Fusce et egestas mauris. Cras rutrum mi eget posuere ultricies. Aliquam auctor vel ipsum at vulputate. Vestibulum est orci, gravida efficitur placerat id, pharetra accumsan tortor. Nunc sodales, mi eget semper fermentum, justo ipsum vehicula ipsum, nec sollicitudin lectus risus ac erat. Proin massa ante, faucibus et metus in, faucibus ullamcorper odio. Integer lacus ligula, fermentum sed faucibus non, faucibus eget nunc. Cras sed ante nec tellus condimentum iaculis. Nulla ac consectetur tellus. Vivamus pulvinar mattis tincidunt.", /*#__PURE__*/_react.default.createElement("br", null), /*#__PURE__*/_react.default.createElement("br", null), "Ut quis nibh ac ligula hendrerit aliquam. Praesent auctor erat eu feugiat congue. Nunc sollicitudin sapien vitae tellus pharetra rhoncus vestibulum ac velit. Aenean ut massa scelerisque, feugiat tellus in, facilisis sapien. Donec metus ex, luctus a mi sed, aliquam bibendum lectus. Donec tincidunt ipsum sit amet nisi pharetra tincidunt. Curabitur ut sapien est. Donec sit amet tempor justo. Donec lacinia tortor non elit rhoncus, eu porttitor magna auctor. Etiam tincidunt massa in iaculis molestie. Nulla accumsan ligula ac maximus congue. "));
};

exports.p = p;