"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.P = exports.H4 = exports.H3 = exports.H2 = exports.H1 = void 0;

var _react = _interopRequireWildcard(require("react"));

require("./Text.css");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var H1 = /*#__PURE__*/function (_Component) {
  _inherits(H1, _Component);

  var _super = _createSuper(H1);

  function H1() {
    _classCallCheck(this, H1);

    return _super.apply(this, arguments);
  }

  _createClass(H1, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("h1", {
        className: "gd-h1"
      }, this.props.children);
    }
  }]);

  return H1;
}(_react.Component);

exports.H1 = H1;

var H2 = /*#__PURE__*/function (_Component2) {
  _inherits(H2, _Component2);

  var _super2 = _createSuper(H2);

  function H2() {
    _classCallCheck(this, H2);

    return _super2.apply(this, arguments);
  }

  _createClass(H2, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("h2", {
        className: "gd-h2"
      }, this.props.children);
    }
  }]);

  return H2;
}(_react.Component);

exports.H2 = H2;

var H3 = /*#__PURE__*/function (_Component3) {
  _inherits(H3, _Component3);

  var _super3 = _createSuper(H3);

  function H3() {
    _classCallCheck(this, H3);

    return _super3.apply(this, arguments);
  }

  _createClass(H3, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("h3", {
        className: "gd-h3"
      }, this.props.children);
    }
  }]);

  return H3;
}(_react.Component);

exports.H3 = H3;

var H4 = /*#__PURE__*/function (_Component4) {
  _inherits(H4, _Component4);

  var _super4 = _createSuper(H4);

  function H4() {
    _classCallCheck(this, H4);

    return _super4.apply(this, arguments);
  }

  _createClass(H4, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("h4", {
        className: "gd-h4"
      }, this.props.children);
    }
  }]);

  return H4;
}(_react.Component);

exports.H4 = H4;

var P = /*#__PURE__*/function (_Component5) {
  _inherits(P, _Component5);

  var _super5 = _createSuper(P);

  function P() {
    _classCallCheck(this, P);

    return _super5.apply(this, arguments);
  }

  _createClass(P, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("p", {
        className: "gd-paragraph"
      }, this.props.children);
    }
  }]);

  return P;
}(_react.Component);

exports.P = P;