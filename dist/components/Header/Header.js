"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BigHeaderSubtitle = exports.BigHeader = exports.HeaderStat = exports.HeaderStatGroup = exports.HeaderTitle = exports.HeaderPretitle = exports.HeaderColumn = exports.Header = void 0;

var _react = _interopRequireWildcard(require("react"));

require("./Header.css");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var HeaderColumn = /*#__PURE__*/function (_Component) {
  _inherits(HeaderColumn, _Component);

  var _super = _createSuper(HeaderColumn);

  function HeaderColumn() {
    _classCallCheck(this, HeaderColumn);

    return _super.apply(this, arguments);
  }

  _createClass(HeaderColumn, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-header-column"
      }, this.props.children);
    }
  }]);

  return HeaderColumn;
}(_react.Component);

exports.HeaderColumn = HeaderColumn;

var HeaderPretitle = /*#__PURE__*/function (_Component2) {
  _inherits(HeaderPretitle, _Component2);

  var _super2 = _createSuper(HeaderPretitle);

  function HeaderPretitle() {
    _classCallCheck(this, HeaderPretitle);

    return _super2.apply(this, arguments);
  }

  _createClass(HeaderPretitle, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-header-pretitle"
      }, this.props.children);
    }
  }]);

  return HeaderPretitle;
}(_react.Component);

exports.HeaderPretitle = HeaderPretitle;

var HeaderTitle = /*#__PURE__*/function (_Component3) {
  _inherits(HeaderTitle, _Component3);

  var _super3 = _createSuper(HeaderTitle);

  function HeaderTitle() {
    _classCallCheck(this, HeaderTitle);

    return _super3.apply(this, arguments);
  }

  _createClass(HeaderTitle, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-header-title"
      }, this.props.children);
    }
  }]);

  return HeaderTitle;
}(_react.Component);

exports.HeaderTitle = HeaderTitle;

var HeaderStatGroup = /*#__PURE__*/function (_Component4) {
  _inherits(HeaderStatGroup, _Component4);

  var _super4 = _createSuper(HeaderStatGroup);

  function HeaderStatGroup() {
    _classCallCheck(this, HeaderStatGroup);

    return _super4.apply(this, arguments);
  }

  _createClass(HeaderStatGroup, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-header-stat-box"
      }, this.props.children);
    }
  }]);

  return HeaderStatGroup;
}(_react.Component);

exports.HeaderStatGroup = HeaderStatGroup;

var HeaderStat = /*#__PURE__*/function (_Component5) {
  _inherits(HeaderStat, _Component5);

  var _super5 = _createSuper(HeaderStat);

  function HeaderStat() {
    _classCallCheck(this, HeaderStat);

    return _super5.apply(this, arguments);
  }

  _createClass(HeaderStat, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-header-stat"
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-header-stat-subtitle"
      }, this.props.sub), /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-header-stat-value"
      }, this.props.val));
    }
  }]);

  return HeaderStat;
}(_react.Component);

exports.HeaderStat = HeaderStat;

var Header = /*#__PURE__*/function (_Component6) {
  _inherits(Header, _Component6);

  var _super6 = _createSuper(Header);

  function Header() {
    _classCallCheck(this, Header);

    return _super6.apply(this, arguments);
  }

  _createClass(Header, [{
    key: "render",
    value: function render() {
      var styles = {
        backgroundImage: "url(" + this.props.bkg + ")"
      };
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-header ".concat(this.props.darkBkg ? 'gd-header-dark' : 'gd-header-light'),
        style: styles
      }, this.props.children);
    }
  }]);

  return Header;
}(_react.Component);

exports.Header = Header;

var BigHeader = /*#__PURE__*/function (_Component7) {
  _inherits(BigHeader, _Component7);

  var _super7 = _createSuper(BigHeader);

  function BigHeader() {
    _classCallCheck(this, BigHeader);

    return _super7.apply(this, arguments);
  }

  _createClass(BigHeader, [{
    key: "render",
    value: function render() {
      var styles = {
        backgroundImage: "url(" + this.props.bkg + ")"
      };
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-big-header ".concat(this.props.darkBkg ? 'gd-header-dark' : 'gd-header-light'),
        style: styles
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-big-header-content"
      }, this.props.children));
    }
  }]);

  return BigHeader;
}(_react.Component);

exports.BigHeader = BigHeader;

var BigHeaderSubtitle = /*#__PURE__*/function (_Component8) {
  _inherits(BigHeaderSubtitle, _Component8);

  var _super8 = _createSuper(BigHeaderSubtitle);

  function BigHeaderSubtitle() {
    _classCallCheck(this, BigHeaderSubtitle);

    return _super8.apply(this, arguments);
  }

  _createClass(BigHeaderSubtitle, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react.default.createElement("div", {
        className: "gd-header-subtitle"
      }, this.props.children);
    }
  }]);

  return BigHeaderSubtitle;
}(_react.Component);

exports.BigHeaderSubtitle = BigHeaderSubtitle;