"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.basic = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _GambiApp = _interopRequireDefault(require("../../page/App/GambiApp"));

var _react2 = require("@storybook/react");

var _addonInfo = require("@storybook/addon-info");

var _Menu = _interopRequireWildcard(require("./Menu"));

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _react2.addDecorator)(_addonInfo.withInfo); // Place outside of router

var _default = {
  title: 'Menu',
  decorators: [_addonInfo.withInfo]
};
exports.default = _default;

var basic = function basic() {
  return /*#__PURE__*/_react.default.createElement(_GambiApp.default, null, /*#__PURE__*/_react.default.createElement(_Menu.default, {
    entries: [{
      "Title": "Home",
      "Loc": "/"
    }, {
      "Title": "About",
      "Loc": "/about"
    }, {
      "Title": "External",
      "Loc": "https://example.com"
    }],
    callback: function callback(val) {
      alert(val);
    }
  }, /*#__PURE__*/_react.default.createElement("p", null, "You are responsible for writing a callback to handle routing")));
};

exports.basic = basic;