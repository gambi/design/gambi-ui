import React, {Component} from "react";
import "./Input.css"
import BottomDrawer from "../BottomDrawer/BottomDrawer";
import Button from "../Button/Button";
import List from "../List/List";


class Input extends Component {


    constructor(props) {
        super(props);
        if(this.props.callback){
            this.cb = this.props.callback.bind(this);
        }else{
            this.cb = this.emptyFn.bind(this);
        }
    }
    emptyFn(val){
        console.warn(val);
    }

    handleChange(event){

        this.cb(event.target.value);
    }

    render() {
        if (this.props.placeholder != null || this.props.placeholder == "") {
            this.placeholder = ""
        } else {
            this.placeholder = this.props.placeholder;
        }
        if (this.props.black) {
            this.theme = "gd-color-black";
        } else {
            this.theme = "gd-color-primary";

        }
        if(this.props.secure){
            this.type = "password";
        } else if (this.props.num){
            this.type = "number";
        }else{
            this.type = "text"
        }


        return (
            <input type={"text"} className={`gd-text-input ${this.theme}`} placeholder={this.props.placeholder} type={this.type} onChange={(val)=>{this.handleChange(val);}}/>
        )
    }
}

class Spinner extends Component {
    constructor(props) {
        super(props);
        this.state = {open: false, val: [], listKey: Math.floor(Math.random()*999999)};
        this.onFinish = this.closeSpinner.bind(this);
        this.onStart = this.openSpinner.bind(this);
        this.cols = this.props.cols;
        if(this.props.title){
            this.title = this.props.title;
        }else{
            this.title = "Open";
        }

        this.cb = this.updateVal.bind(this);

        if(this.props.multi){
            this.mi = this.props.multi;
        }else{
            this.mi = [];
        }

        if(this.props.onUpdate){
            this.callback = this.props.onUpdate.bind(this);
        }else{
            this.callback = this.emptyFun.bind(this);
        }
    }

    emptyFun(){
        console.warn("Update Method not Implemented")
    }

    closeSpinner() {

        this.callback(this.state.val);

        this.setState({
            open: false,
        })

        var a = setTimeout(function(){
            this.setState({
                listKey: Math.floor(Math.random()*999999)

            });
        }.bind(this), 1050);

    }

    openSpinner() {

        if(this.props.clearOnOpen){
            var a = this.state.val;
            while (a.length) {
                a.pop();
            }
            console.log(a);

            this.setState({
                open: true,
                val: a,
            })
        }
        this.setState({
            open: true
        })
    }

    updateVal(index, val){
        var aval = this.state.val;
        if(val[0] !== this.state.val[0] && val[0] !== undefined){
            aval[index] = val;
            this.state.val = aval;

        }




    }



    render() {
        return (
            <div>
                <Button black outline onClick={this.onStart}>{this.title}</Button>
                <BottomDrawer open={this.state.open} onFinished={this.onFinish} key={this.state.listKey}>
                    <div className={"gd-spinner-container"}>

                        {this.props.cols.map((col, index) => {
                            return(
                            <div className={"gd-spinner-col"} key={index}>
                                <List options={col} selectable multi={this.mi.includes(index)} onUpdate={(val) => {this.cb(index,val);}}/>

                            </div>);

                        })}
                    </div>

                </BottomDrawer>
            </div>

        )
    }
}

class Slider extends Component {

    constructor(props) {
        super(props);
        this.setState({value: (this.props.max + this.props.min) / 2});

        this.onChange = this.onChange.bind(this)

        this.listen = props.onUpdate.bind(this)

    }

    onChange(event) {
        this.listen(event.target.value)
        this.setState({
            value: event.target.value
        });
    }

    render() {
        return (
            <div className="slidecontainer">
                <input type="range" min={this.props.min} max={this.props.max} value={this.state.value}
                       className={"slider"} onChange={this.onChange}/>
            </div>
        );
    }
}

export {
    Input,
    Slider,
    Spinner
}