import GambiApp from "../../page/App/GambiApp";
import React from "react";

import { addDecorator } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import { setConsoleOptions } from '@storybook/addon-console';
import {Input, Slider, Spinner} from "./Input";

setConsoleOptions({
    panelExclude: [],
});

addDecorator(withInfo)
// Place outside of router

export default { title: 'Inputs', decorators: [withInfo] };
export const input = () => <GambiApp>
    <Input placeholder={"Enter text"} callback={(val)=>{console.log(val);}}/>
    <br />
    <br />
    <Input black/>
</GambiApp>

export const secure = () => <GambiApp>
    <Input placeholder={"Enter text"} secure/>

</GambiApp>

export const singleColumnSpinner = () => <GambiApp>
    <Spinner cols={[
        [
            "Q1",
            "Q2",
            "Q3",
            "Q4"
        ]
    ]} onUpdate={(val) => {console.log(val);}}></Spinner>
</GambiApp>

export const clearOnOpen = () => <GambiApp>
    <Spinner cols={[
        [
            "Q1",
            "Q2",
            "Q3",
            "Q4"
        ]
    ]} onUpdate={(val) => {console.log(val);}} clearOnOpen title={"Clears on Open"}></Spinner>
</GambiApp>


export const multiColumnSpinner = () => <GambiApp>
    <Spinner cols={[
        [
            "Q1",
            "Q2",
            "Q3",
            "Q4"
        ],
        [
            "2020",
            "2019",
            "2018",
            "2017"
        ]
    ]} onUpdate={(val) => {console.log(val);}}></Spinner>
</GambiApp>

export const multiSelectSpinner = () => <GambiApp>
    <Spinner cols={[
        [
            "Q1",
            "Q2",
            "Q3",
            "Q4"
        ],
        [
            "2020",
            "2019",
            "2018",
            "2017"
        ]
    ]} multi={[0, 1]} onUpdate={(val) => {console.log(val);}}></Spinner>
</GambiApp>
export const slider = () => <GambiApp>
    <Slider min={0} max={1000} onUpdate={(value) => {console.clear();console.log(value);}}/>
    <p>Open the console to view the output data</p>
</GambiApp>