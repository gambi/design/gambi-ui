import React from 'react';
import {
    BigHeader,
    BigHeaderSubtitle,
    Header,
    HeaderColumn,
    HeaderPretitle,
    HeaderStat,
    HeaderStatGroup,
    HeaderTitle
} from './Header';
import GambiApp from "../../page/App/GambiApp";

import { addDecorator } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

addDecorator(withInfo)

export default { title: 'Header', decorators: [withInfo] };
export const basic = () => <GambiApp>
    <Header bkg={"https://images.unsplash.com/photo-1557682260-96773eb01377"}>
        <HeaderTitle>Title</HeaderTitle>
    </Header>
</GambiApp>

export const allOptions= () => <GambiApp>
    <Header bkg={"https://images.unsplash.com/photo-1579548122080-c35fd6820ecb"} darkBkg={true}>
        <HeaderColumn>
            <HeaderPretitle>
                Pre-title
            </HeaderPretitle>
            <HeaderTitle>
                Title
            </HeaderTitle>
        </HeaderColumn>
        <HeaderColumn>
            <HeaderStatGroup>
                <HeaderStat val="123" sub="Sub" />
                <HeaderStat val="123" sub="Sub" />

            </HeaderStatGroup>
        </HeaderColumn>
    </Header>

</GambiApp>;

    export const bigHeader = () => <GambiApp>
        <BigHeader bkg={"https://images.unsplash.com/photo-1579548122080-c35fd6820ecb"} darkBkg={true}>
            <HeaderTitle>Hello</HeaderTitle>
            <BigHeaderSubtitle>There</BigHeaderSubtitle>
        </BigHeader>

    </GambiApp>