import React from 'react';
import GambiApp from "../../page/App/GambiApp";
import {H1, H2, H3, H4, P} from "./Text";
import { addDecorator } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

addDecorator(withInfo)

export default { title: 'Text', decorators: [withInfo] };
export const h1 = () => <GambiApp>
    <H1>Header 1</H1>
</GambiApp>

export const h2 = () => <GambiApp>
    <H2>Header 2</H2>
</GambiApp>

export const h3 = () => <GambiApp>
    <H3>Header 3</H3>
</GambiApp>

export const h4 = () => <GambiApp>
    <H4>Header 4</H4>
</GambiApp>

export const p = () => <GambiApp>
    <P>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum laoreet urna quis nulla gravida varius. In aliquet tristique enim at bibendum. Sed lectus diam, aliquam id hendrerit sit amet, interdum non mauris. Suspendisse interdum turpis in quam interdum, vitae iaculis sapien venenatis. Donec non sem eu augue volutpat auctor. Quisque ac lacus vel metus finibus viverra. Donec ac sagittis ante, sed suscipit dui. Curabitur sit amet consectetur mauris. Nulla facilisi. Duis sed vestibulum massa. Curabitur sodales iaculis viverra.
        <br />
        <br />

        Phasellus orci nulla, mollis in dolor tincidunt, mollis posuere ipsum. Pellentesque at pulvinar quam, vitae aliquet velit. Aenean commodo sapien quis turpis faucibus, vitae mollis orci aliquam. Proin nulla odio, malesuada in ultrices vel, porttitor tristique felis. Sed tempus, purus nec sollicitudin blandit, felis lacus bibendum odio, non pellentesque justo diam eget lacus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam erat volutpat. Phasellus in cursus libero, in auctor felis. Quisque volutpat, velit vel tristique interdum, velit odio rhoncus turpis, quis vehicula magna arcu id odio. Sed auctor massa vitae felis scelerisque, quis consequat odio rhoncus. Vestibulum laoreet est enim. Nam accumsan feugiat semper. Praesent tincidunt faucibus neque, at malesuada ex dictum vitae. Duis placerat mollis arcu eget suscipit.
        <br />
        <br />

        Quisque mauris ligula, condimentum at sapien in, tempus mollis dolor. Aliquam erat volutpat. Morbi venenatis auctor odio, ac ultrices justo rhoncus posuere. Pellentesque sit amet velit dignissim, viverra tellus eget, vulputate ex. Proin iaculis a mi eget dignissim. Praesent risus sapien, pellentesque sed orci sed, mattis faucibus metus. Etiam commodo purus non purus iaculis, vitae tincidunt ante interdum. Nulla scelerisque facilisis tortor sed laoreet. Ut orci ligula, sodales id elementum id, maximus in elit. Cras sodales condimentum dictum. Quisque egestas dolor velit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        <br />
        <br />

        Cras et libero nec elit laoreet tincidunt at accumsan justo. Vestibulum diam nisi, convallis in condimentum quis, auctor nec urna. Aenean volutpat accumsan nisi sed varius. Fusce et egestas mauris. Cras rutrum mi eget posuere ultricies. Aliquam auctor vel ipsum at vulputate. Vestibulum est orci, gravida efficitur placerat id, pharetra accumsan tortor. Nunc sodales, mi eget semper fermentum, justo ipsum vehicula ipsum, nec sollicitudin lectus risus ac erat. Proin massa ante, faucibus et metus in, faucibus ullamcorper odio. Integer lacus ligula, fermentum sed faucibus non, faucibus eget nunc. Cras sed ante nec tellus condimentum iaculis. Nulla ac consectetur tellus. Vivamus pulvinar mattis tincidunt.
        <br />
        <br />

        Ut quis nibh ac ligula hendrerit aliquam. Praesent auctor erat eu feugiat congue. Nunc sollicitudin sapien vitae tellus pharetra rhoncus vestibulum ac velit. Aenean ut massa scelerisque, feugiat tellus in, facilisis sapien. Donec metus ex, luctus a mi sed, aliquam bibendum lectus. Donec tincidunt ipsum sit amet nisi pharetra tincidunt. Curabitur ut sapien est. Donec sit amet tempor justo. Donec lacinia tortor non elit rhoncus, eu porttitor magna auctor. Etiam tincidunt massa in iaculis molestie. Nulla accumsan ligula ac maximus congue. </P>
</GambiApp>

