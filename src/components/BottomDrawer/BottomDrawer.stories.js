import GambiApp from "../../page/App/GambiApp";
import React from "react";

import { addDecorator } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import { setConsoleOptions } from '@storybook/addon-console';
import BottomDrawer from "./BottomDrawer";

setConsoleOptions({
    panelExclude: [],
});

addDecorator(withInfo)
// Place outside of router

export default { title: 'Drawer', decorators: [withInfo] };

export const Drawer = () => <GambiApp>
    <BottomDrawer open>Hello</BottomDrawer>
</GambiApp>