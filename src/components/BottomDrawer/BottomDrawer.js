import React, {Component} from "react";
import "./BottomDrawer.css";
class BottomDrawer extends Component{
    constructor(props) {
        super(props);
        this.state = {};

        if(this.props.onFinished != null){
            this.state.closeFn = this.props.onFinished.bind(this);
        }else{
            this.state.closeFn = this.emptyFn.bind(this);
        }


    }
    emptyFn(){
        console.warn("Bottom Drawer OnClosed Not Implemented")
    }
    render() {
        return (
            <div>
                <div className={`gd-bottom-drawer-back ${this.props.open ? "gd-bottom-drawer-back-open" : ""}`} onClick={this.state.closeFn}>


                </div>
                <div className={`gd-bottom-drawer ${this.props.open ? "gd-bottom-drawer-open" : ""}`}>
                    {this.props.children}
                </div>
            </div>

        );
    }
}

export default BottomDrawer;