import React, {Component} from "react";
import "./Button.css";
export default class Button extends Component{

    constructor(props) {
        super(props);

        if(props.onClick != null){
            this.listen = props.onClick.bind(this);

        }else{
            this.listen = this.blankOnClick.bind(this);
        }

        if(this.props.black){
            this.state = {theme: "gd-button-black"};
        }else{
            this.state = {theme: "gd-button-primary"};

        }

    }

    blankOnClick(){
        console.warn("Callback not implemented")
    }
    render() {

        return (
            <button className={`gd-button ${this.props.large ? "gd-button-large" : ""} ${this.props.small ? "gd-button-small" : ""} ${this.props.outline ? "gd-button-outline" : "gd-button-fill"} ${this.props.rounded ? "gd-button-rounded" : ""} ${this.props.pill ? "gd-button-pill" : ""} ${this.props.full ? "gd-button-full":""} ${this.state.theme}`} onClick={this.listen}>{this.props.children}</button>
        );
    }
}
