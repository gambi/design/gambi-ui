import React from 'react';
import GambiApp from "../../page/App/GambiApp";

import { addDecorator } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';
import Menu, {MenuEntry} from "./Menu";

addDecorator(withInfo)
// Place outside of router

export default { title: 'Menu', decorators: [withInfo] };

export const basic = () => <GambiApp>
    <Menu entries={[
        {
            "Title":"Home",
            "Loc":"/"
        },
        {
            "Title":"About",
            "Loc":"/about"
        },
        {
            "Title":"External",
            "Loc":"https://example.com"
        }
    ]} callback={(val)=>{alert(val);}}>
        <p>You are responsible for writing a callback to handle routing</p>
    </Menu>
</GambiApp>