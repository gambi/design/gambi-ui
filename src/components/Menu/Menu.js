import React,{Component} from "react";
import "./Menu.css";
import { createElement } from "glamor/react";

// Based on https://codesandbox.io/s/github/wizardry-io/animated-modal/tree/1cdedfe4ad2ad728f4226ba896c5f3a5bb2fb850/?from-embed=&file=/src/App.js:42-88
/* @jsx createElement */

const redColor = "#ED5565";
//const mediumColor = "#ED5565";
const mediumColor = "#F66459";
const orangeColor = "#FC6E51";
const lightGrey = "#DCDAE1";
const black = "#333";
const yellow = "#E9B308";
const white = "#FFF";
const beige = "#EAB49E";
const transparentWhite = "rgba(255,255,255,.9)";

const Dot = () => (
    <div
        css={{
            width: 2,
            height: 2,
            backgroundColor: lightGrey,
            borderRadius: 2,
            marginTop: 2
        }}
    />
);



class MenuEntry extends Component{
    constructor(props) {
        super(props);
        if(this.props.transitionDuration){
            this.transitionDuration = this.props.transitionDuration;
        }else{
            this.transitionDuration = 1;

        }
        if(this.props.callback != undefined){
            this.cb = this.props.callback.bind(this);

        }else{
            this.cb = this.emptyFn.bind(this);
        }
        this.transitionDelay = 0.25;

    }
    emptyFn(){
        console.warn("Callback not implemented for MenuEntry");
    }
    render() {
        return (
            <div
                css={{
                    zIndex: 1,
                    transitionProperty: "transform",
                    transitionDuration: `${this.transitionDuration}s`,
                    transitionDelay: this.transitionDelay,
                    transform: `translateY(${this.props.isOpen ? 0 : 20}px)`,
                    listStyleType: "none"
                    //fontSize: "16pt"
                }}
                key={this.props.menuItem["Title"]}
            >
                <a
                    css={{
                        cursor: "pointer",
                        opacity: this.props.isOpen ? 1 : 0,
                        transitionProperty: "opacity",
                        transitionDuration: `${this.transitionDuration}s`,
                        transitionDelay: this.transitionDelay,
                        zIndex: 1,
                        color: white
                    }}
                    className="gd-menu-item"

                    onClick={()=>{this.cb(this.props.menuItem["Loc"]);}}

                >
                    {this.props.menuItem["Title"]}
                </a>
            </div>
        );
    }
}

class GambiMenu extends Component{

    constructor(props) {
        super(props);
        this.transitionDuration = 1;
        if(this.props.transitionDuration){
           this.transitionDuration = this.props.transitionDuration;
        }
        this.cb = this.props.callback.bind(this);
    }
    render() {
        return(<div
            css={{
                position: "absolute",
                left: 0,
                top: 0,
                width: "100%",
                height: "100%",
                overflow: "hidden",
                pointerEvents: this.props.isOpen ? "initial" : "none",
                zIndex: 2
            }}
        >
            <div
                css={{
                    display: "grid",
                    flexDirection: "column",
                    paddingTop: 40,
                    paddingLeft: 50,
                    gridGap: 25,
                }}
            >
                {this.props.menuItems.map((menuItem, index) => {
                    return <MenuEntry transitionDuration={this.transitionDuration} index={index} isOpen={this.props.isOpen} menuItem={menuItem} key={menuItem["Title"]} callback={this.cb}/>;
                })}
            </div>

            <div
                css={{
                    position: "absolute",
                    willChange: "transform",
                    top: 0,
                    left: 0,
                    backgroundColor: transparentWhite,
                    width: "220vh",
                    height: "220vh",
                    transform: `translate(-50%,-50%) ${this.props.isOpen ? "scale(1)" : "scale(0)"}`,
                    transitionProperty: "transform",
                    transitionDuration: `${this.transitionDuration / 2}s`,
                    transitionDelay: !this.props.isOpen && `${this.transitionDuration / 2}s`,
                    borderRadius: "100%"
                }}
            />

            <a
                id="gd-menu-back"
                css={{
                    width: "100vw",
                    height: "100vh",
                    position: "absolute",
                    top: 0,
                    left: 0,
                    padding: 0,
                    margin: 0,

                    backgroundColor: transparentWhite,
                    zIndex: 0,
                    transform: `scale(${this.props.isOpen ? "100%" : "0"}, ${this.props.isOpen ? "100%" : "0"})`,
                    transitionProperty: "scale",
                    transitionDuration: `${this.transitionDuration}s`,
                    transitionTimingFunction: this.props.isOpen
                        ? "cubic-bezier(0.5, 1, 0.3, 1.3)"
                        : "cubic-bezier(0.5, -1, 0.3, 1.3)",
                    cursor: "pointer"
                }}
                onClick={this.props.onCloseButtonClick}
            />
            <div
                css={{
                    position: "absolute",
                    willChange: "transform",
                    top: "-20vh",
                    marginTop: 45,
                    left: 0,
                    backgroundColor: mediumColor,
                    width: "calc(140vh + 20vh * 2)",
                    height: "calc(140vh + 20vh * 2)",
                    transform: `translate(-50%,-50%) ${this.props.isOpen ? "scale(1)" : "scale(0)"}`,
                    transitionProperty: "transform",
                    transitionDuration: `${this.transitionDuration / 2}s`,
                    transitionDelay: !this.props.isOpen && `${this.transitionDuration / 2}s`,
                    borderRadius: "100%"
                }}
            />
            <div
                css={{
                    position: "absolute",
                    willChange: "transform",
                    top: 40,
                    left: 40,
                    backgroundColor: redColor,
                    width: "calc(70vh*2)",
                    height: "calc(70vh*2)",
                    transform: `translate(-50%,-50%) ${this.props.isOpen ? "scale(1)" : "scale(0)"}`,
                    transitionProperty: "transform",
                    transitionDuration: `${this.transitionDuration / 2}s`,
                    transitionDelay: !this.props.isOpen && `${this.transitionDuration / 2}s`,
                    borderRadius: "100%"
                }}
            />
        </div>);
    }
}


class Menu extends Component {

    constructor(props) {
        super(props);
        this.state = { isMenuOpened: false };

        this.items = this.props.entries;
        if(this.props.callback != undefined){
            this.cb = this.props.callback.bind(this);
        }else{
            this.cb = this.emptyFn.bind(this);
        }
    }
    emptyFn(val){
        console.warn("Method not implemented in menu");
        console.info(`Val: ${val}`);
    }
    render() {
        console.log(this.cb)
        return (
            <div
            >
                <a
                    id="MenuBtn"
                    onClick={() => this.setState({ isMenuOpened: true })}
                    css={{
                        backgroundColor: redColor,
                        width: 40,
                        height: 40,
                        position: "absolute",
                        top: 20,
                        left: 20,
                        borderRadius: 20,
                        boxShadow: `2.5px 2.5px 10px 1px ${orangeColor}`,
                        cursor: "pointer",

                    }}
                >
                    < div className="gd-menu-icon">
                        <span className="material-icons">
apps
</span>
                    </div>
                    </a>
                <GambiMenu
                    isOpen={this.state.isMenuOpened}
                    menuItems={this.items}
                    onCloseButtonClick={() => this.setState({ isMenuOpened: false })}
                    transitionDuration={.75}
                    callback={this.cb}
                />
                {this.props.children}
            </div>
        );
    }
}

export default Menu;
