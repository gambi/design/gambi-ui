import React, {Component} from "react";
import './List.css';
class ListItem extends Component{
    constructor(props) {
        super(props);
        this.onClick = this.props.onClick.bind(this);
    }
    render() {
        return(
            <div className={`gd-list-item ${this.props.active?"gd-list-item-selected":""}`} onClick={()=>this.onClick(this.props.title)}>
            {this.props.active? <span className="material-icons">done</span> : ""}
            <span className={"gd-list-item-title"}>{this.props.title}</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span>{this.props.children}</span>
        </div>
        );
    }
}
export default class List extends Component {




    state = {
        multiSelect: false,
        updates: 0,
        selected: []
    }
    constructor(props) {
        super(props);

        this.oc = this.itemClicked.bind(this)

        if(this.props.onUpdate != null){
            this.onUpdate = this.props.onUpdate.bind(this);
        }else{
            this.onUpdate = this.blankFn.bind(this);
        }
        if(this.props.addElements){
            this.addElements = this.props.addElements.bind(this);
        }else{
            this.addElements = this.blankButtons.bind(this);
        }
    }
    filterRemove(list, val) {
        var filteredItems = list.filter(item => item !== val)
        return filteredItems;
    }

    itemClicked(value){

        if(this.props.selectable){
            if(this.props.multi){
                if(this.state.selected.includes(value)){
                    this.setState({
                        selected: this.filterRemove(this.state.selected, value),
                        updates: this.state.updates + 1
                    })
                }else{
                    this.state.selected.push(value)
                    this.setState({
                        updates: this.state.updates + 1
                    })
                }
            }else{
                if(this.state.selected.includes(value)){

                    this.setState({
                        selected: []
                    })
                }else{
                    this.setState({
                        selected: value
                    });
                }

            }


        }
        this.onUpdate(value);
    }



    blankFn(){
        console.warn("List on click method not implemented")
    }

    blankButtons(){

    }
    render() {

        return (
            <div className={"gd-list"}>
                {this.props.options.map((option, index) => {
                    return(<ListItem key={option} active={this.state.selected.includes(option)} onClick={this.oc} title={option}>{this.addElements(index, option)}</ListItem>);

                })}
            </div>
        );
    }
}
export {
    ListItem
}