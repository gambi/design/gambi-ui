import GambiApp from "../../page/App/GambiApp";
import React from "react";

import { addDecorator } from '@storybook/react';
import { withInfo } from '@storybook/addon-info';

import { setConsoleOptions } from '@storybook/addon-console';
import List from "./List";
import Button from "../Button/Button";

setConsoleOptions({
    panelExclude: [],
});

addDecorator(withInfo)
// Place outside of router

export default { title: 'List', decorators: [withInfo] };

export const staticList = () => <GambiApp>
    <List options={[
        "São Paulo",
        "Rio de Janeiro",
        "Brasília",
        "Salvador",
        "Fortaleza"
    ]} />
</GambiApp>

export const selectable = () => <GambiApp>
    <List options={[
        "São Paulo",
        "Rio de Janeiro",
        "Brasília",
        "Salvador",
        "Fortaleza"
    ]} selectable/>
</GambiApp>

export const multiSelectable = () => <GambiApp>
    <List options={[
        "São Paulo",
        "Rio de Janeiro",
        "Brasília",
        "Salvador",
        "Fortaleza"
    ]} multi selectable/>
</GambiApp>

export const callback = () => <GambiApp>
    <List options={[
        "São Paulo",
        "Rio de Janeiro",
        "Brasília",
        "Salvador",
        "Fortaleza"
    ]} selectable onUpdate={(val) => console.log(val)}/>
</GambiApp>

export const withOptions = () => <GambiApp>
    <List options={[
        "São Paulo",
        "Rio de Janeiro",
        "Brasília",
        "Salvador",
        "Fortaleza"
    ]} selectable onUpdate={(val) => console.log(val)} addElements={(index, option)=>{return(<Button black outline onClick={()=>{alert(option);}}>{index}</Button>);}}/>
</GambiApp>