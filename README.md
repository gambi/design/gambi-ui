# Gambi UI
> *The Mobile-First React Library*

## Install

```shell script
npm i gambi-ui
```
## Storybook
Check out our components on [Storybook](https://storybook.ui.gambi.design)
